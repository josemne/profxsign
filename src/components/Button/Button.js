import React from 'react'
import PropTypes from 'prop-types'
import './Button.css'

export default function Button(props) {
    const { text } = props;
    return (
        <button className="spx_button">
            {text}
        </button>
    )
}

Button.propTypes = {
    text: PropTypes.string,
    // text: PropTypes.bool,
    // text: PropTypes.number,
    // text: PropTypes.shape({
    //     text: PropTypes.string,
    // }),
    // text: PropTypes.arrayOf(PropTypes.string),
};