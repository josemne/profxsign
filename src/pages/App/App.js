import React from 'react';
import Button from '../../components/Button/Button';
import logo from '../../styles/images/logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
          <Button text={'hello world'}/>
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          we are killing itnpm start
        </a>
      </header>
    </div>
  );
}

export default App;
